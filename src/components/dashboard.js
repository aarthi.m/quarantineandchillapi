import React from "react" ;
import './dashboard.css';
import Cookies from 'js-cookie';
import profile from './profile.js';
import Navbar from './navbar.js';
//import { Button, Col, Row, Container } from 'react-bootstrap';
//import {FormGroup,Input,Card,CardBody,Form} from 'reactstrap';

class dashboard extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            usersData : [ ],
            requestData: [ ],
            buttonText: "Add as Friend",
            receiverId:"",
            receiverName:""
        };
       this.handleSubmit = this.handleSubmit.bind(this) 
    };
    
    handleSubmit (name) {
        return event => {
        event.preventDefault();
        console.log(name);
        this.setState({ buttonText : "Request Sent" });
        
        const data = {
            senderName: Cookies.get('userName'),
            receiverName: name
        };

        fetch('http://localhost:8000/user/friendrequest', {
            method: 'POST',
            headers:{
                'content-type': "application/json"
            //    "x-access-token": Cookies.get('token' ) 
            },
            body : JSON.stringify(data)
        })
        .then((data) => {
            if(data.auth === true){
                alert("Updation Success");
                
            }
         })
         //window.location.href = '/request'   
        }
    }
    // addFriend(receiverName) {
    //     console.log(receiverName);
    // }

    // senddetails(receiverName) {
    //     //e.preventDefault();
    //    //console.log(e);
    //   // alert(receiverName);
    //     this.setState({ buttonText : "Request Sent" });
    
    //     const senderUserId = Cookies.userId;  
    //     const senderUserName = Cookies.userName;

       

    // }

    componentDidMount(){
        fetch('http://localhost:8000/user/dashboard',{
            method: 'GET',
            headers:{
                'contene-type': "application/json",
                // "x-access-token": Cookies.get('token') 
            }
        }).then(response=> response.json())
        .then(dataObj => {
            //console.log(dataObj.data.Users);
            if (dataObj.auth === true){
                //const users[] = dataObj.data.Users
                this.setState({ 
                    usersData : dataObj.data.Users
                })
            }
            //console.log(this.state.usersDaSta);
        })

        fetch('http://localhost:8000/user/request',{
            method: 'GET',
            headers:{
                'content-type': "application/json",
                // "x-access-token": Cookies.get('token') 
            }
        })
        .then(response=> response.json())
        .then(dataObj => {
            //console.log(dataObj);
            if (dataObj.auth === true){
                //const users[] = dataObj.data.Users
                this.setState({ 
                    requestData : dataObj.data.Requests
                })
            }
            console.log(this.state.requestData);
        })
        
    }
    // logout() {
    //     //Cookies.clear();
    //    // window.location.href = '/';
    // }

    render(){
        return(
            //container mt-5 d-flex justify-content-center
            <div class=" container justify-content-center"> 
            <nav aria-label="breadcrumb" class="main-breadcrumb">
                        <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                        </ol>
                    </nav>
            <div className="row">
                <div className="col-3">
                <div class="card mt-3" style = {{ height:  "600px" , width: "250px"}}>
                            <ul class="list-group list-group-flush">
                            <a href="/profile" class="card-link">
                                <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                                    <h6 class="mb-0"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-globe mr-2 icon-inline"><circle cx="12" cy="12" r="10"></circle><line x1="2" y1="12" x2="22" y2="12"></line><path d="M12 2a15.3 15.3 0 0 1 4 10 15.3 15.3 0 0 1-4 10 15.3 15.3 0 0 1-4-10 15.3 15.3 0 0 1 4-10z"></path></svg> Profile </h6>
                                </li>
                            </a>
                           
                            <a href="/request">
                            <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                                <h6 class="mb-0"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-twitter mr-2 icon-inline text-info"><path d="M23 3a10.9 10.9 0 0 1-3.14 1.53 4.48 4.48 0 0 0-7.86 3v1A10.66 10.66 0 0 1 3 4s-4 9 5 13a11.64 11.64 0 0 1-7 2c9 5 20 0 20-11.5a4.5 4.5 0 0 0-.08-.83A7.72 7.72 0 0 0 23 3z"></path></svg>Requests</h6>
                            </li>
                            </a>
                            <a href='/'>
                            <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                                <h6 class="mb-0"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-instagram mr-2 icon-inline text-danger"><rect x="2" y="2" width="20" height="20" rx="5" ry="5"></rect><path d="M16 11.37A4 4 0 1 1 12.63 8 4 4 0 0 1 16 11.37z"></path><line x1="17.5" y1="6.5" x2="17.51" y2="6.5"></line></svg>
                               Logout</h6>
                            </li>
                            </a>
                            </ul>
                </div> 
                </div>
                <div className="col-9">
                    
               
                {this.state.usersData.map((data,key) => {
                    // const receiverId =  data.userId;
                    // const receiverName = data.userName;
                    //console.log(receiverName);
                    //this.setState({ receiverId :  data.userId, receiverName : data.userName})
                    if( data.userName != Cookies.get('userName')){
                        const dataname= data.userName;
                        console.log(dataname);
                        return(
                       
                            <div class="card p-3  mt-3 justify-content-center" style = {{ width : "800px"}} key={key}>
                            <div class="d-flex align-items-center">
                                <div class="image"> <img src="https://images.unsplash.com/photo-1522075469751-3a6694fb2f61?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=80" class="rounded" width="155">
                                    </img> 
                                </div>
                                <div class="ml-3 w-100">
                                    <h4 class="mb-0 mt-0">{data.userName}</h4> <span>posted</span>
                                    <div class="p-2 mt-2 bg-primary d-flex justify-content-between rounded text-white stats">
                                        <div class="d-flex flex-column"> 
                                        <span class="articles">Status</span> 
                                       
                                        <span class="number1">{data.userStatus}</span>  
                                        </div>
                                        {/* <div class="d-flex flex-column"> <span class="followers">Followers</span> <span class="number2">980</span> </div>
                                        <div class="d-flex flex-column"> <span class="rating">Rating</span> <span class="number3">8.9</span> </div> */}
                                    </div>
                                    <div class="button mt-2 pt-3 d-flex flex-row align-items-center">
                                    
                                        
                                        <button class="btn btn-sm btn-outline-primary">Chat</button> 
                                        {/* <button key= {key1} class=" btn btn-sm btn-primary ml-2" type="submit" onClick = { this.handleSubmit( data.userName ) } >{this.state.buttonText}</button>  */} 
                                         {this.state.requestData.map((data1,key1) => {
                                             console.log(data1.receiverName,dataname );
                                            if(data1.receiverName == dataname){
                                                return(
                                                    <button key= {key1} class=" btn btn-sm btn-primary ml-2" type="submit"  >Request Sent</button>  

                                                )

                                            }
                                            else{
                                                return (
                                                    <button key= {key1} class=" btn btn-sm btn-primary ml-2" type="submit" onClick = { this.handleSubmit( dataname ) } >{this.state.buttonText}</button>  

                                                )

                                            }
                                        })} 
                                        
                                    
                                    </div>
                                </div>
                            </div>
                        </div>
                        )
                    }    
                    
                    
                    
                    })}
                </div>
            </div> 
            </div>
        );
    }
}

export default dashboard